

package inventory.mgmt;




public class Stock {
    
int    Chassis;
String Model;
String Make;

public Stock(){
    
}

    
 public Stock(int Chassis,String Make,String Model){
     
     this.Chassis=Chassis;
     this.Make=Make;
     this.Model=Model;
 }  
    
    
 public void setChassis(int Chassis){
     this.Chassis=Chassis;
 }   
    
public int getChassis(){
    return Chassis;
}    

    public String getModel() {
        return Model;
    }

    public void setModel(String Model) {
        this.Model = Model;
    }

    public String getMake() {
        return Make;
    }

    public void setMake(String Make) {
        this.Make = Make;
    }

    @Override
    public String toString() {
        return "Stock{" + "Chassis=" + Chassis + ", Model=" + Model + ", Make=" + Make + '}';
    }

  

    

    

    
   
    
    
    
    
}
