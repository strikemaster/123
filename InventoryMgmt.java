/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package inventory.mgmt;


import java.io.FileInputStream;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author LDitshwane
 */
public class InventoryMgmt extends Application {
    
    
    //stage
    Stage st;
    
    //jdbc
    
    Database db = new Database();
    
    ObservableList<Stock> ride;
    
    //task buttons
    Button bt; 
    Button bt2;
    Button bt3;
    Button bt4;
    
    //list//
    
    HBox hbx;
     TableView<Stock> tb;
    
    
    
    //add//reqest
    GridPane gp;
    Scene add;
    Label lb;
    TextField chasi;
    TextField make;
    TextField model;
    Button ad;
    
    //request
    ToggleGroup tg;
    RadioButton rb;
     RadioButton rb2;
    GridPane gp2;
    Scene add2;
    Label lb2;
    TextField make2;
    TextField model2;
    Button ad2;
     
    
   public static void main(String[]args){
       
       
      launch(args); 
       
   }
    
   
    @Override
   public void start(Stage stg){
      
     stg.setTitle("inventory mgmt");
     
     BorderPane bp = new BorderPane();//parent
     
    

  
     
     //Vbox
     
     VBox vb = new VBox(80);//left
     
     bp.setLeft(vb);
     
     vb.setPadding(new Insets(80,0,0,0));
     vb.getStyleClass().add("vb");
   
     
     bt = new Button("list");
     bt.setPrefSize(100, 50);
     bt.getStyleClass().add("bt");
     
     bt.setOnAction(new EventHandler(){

         @Override
         public void handle(Event event) {
             
        
         bp.setCenter(tb);
         
             
             
         }
       
     });
     
     
     
     
     
     bt2 = new Button("search");
     bt2.setPrefSize(100, 50);
     bt2.getStyleClass().add("bt");
     
     bt3 = new Button("add");
     bt3.setPrefSize(100, 50);
     bt3.getStyleClass().add("bt");
     bt3.setOnAction(new EventHandler(){
         
         public void handle(Event t){
             
             
            bp.setCenter(gp);
             
         }
         
         
     });
   
     
     
     bt4 = new Button("request");
     bt4.setPrefSize(100, 50);
     bt4.getStyleClass().add("bt");
     bt4.setOnAction(new EventHandler(){
         
         public void handle(Event t){
             
             bp.setCenter(gp2);
             
             
         }
         
         
         
     });
     
     
     vb.getChildren().addAll(bt,bt2,bt3,bt4);
     
    
     
     //titlebar
     StackPane hb = new StackPane();//top
     
     bp.setTop(hb);
   
     hb.getStyleClass().add("hb");
    
     Text txt = new Text("Solaris");
     
     txt.getStyleClass().add("tx");
    
     hb.getChildren().add(txt);
     
     
     //default center
     
     try{ 
    Image img = new Image(new FileInputStream("C:\\Users\\LDitshwane\\Pictures\\black.jpg"));
    
    ImageView vi = new ImageView(img);
    
    vi.setFitWidth(1024);
    vi.setFitHeight(720);
    
     Group grp = new Group();
     
     bp.setCenter(grp);
     
     grp.getChildren().add(vi);
     
     }catch(Exception e){
         
         System.out.println(e);
         
     }
   
  //list scene 
     
    tb = new TableView();
    
    TableColumn chas = new TableColumn("chassis number");
    
    TableColumn mak = new TableColumn("vehicle make");
    
    TableColumn mod = new TableColumn("vehicle model");
    
    chas.setMinWidth(100);
    mak.setMinWidth(100);
    mod.setMinWidth(100);
   
    chas.setCellValueFactory(new PropertyValueFactory("Chassis"));
    mak.setCellValueFactory(new PropertyValueFactory("Make"));
    mod.setCellValueFactory(new PropertyValueFactory("Model"));
    
    tb.getColumns().addAll(chas,mak,mod);
     

    
    tb.setItems(db.getCars());
     

     
     //add scene
     
         
     gp = new GridPane();;//center
     
     gp.getStyleClass().add("gp");
     
    
     gp.setPadding(new Insets(50));
     
     gp.setHgap(15.0);
     gp.setVgap(5.0);
     
     lb = new Label("Enter car details");
     GridPane.setConstraints(lb,20,2);
     lb.getStyleClass().add("tx");
     
     
     chasi = new TextField();
     chasi.setPromptText("enter chassis number");
     chasi.setFocusTraversable(false);
     GridPane.setConstraints(chasi, 20, 10);
     
     make = new TextField();
     make.setPromptText("enter car make");
     make.setFocusTraversable(false);
     GridPane.setConstraints(make, 20, 15);
     
     model = new TextField();
     model.setPromptText("enter car model");
     model.setFocusTraversable(false);
     GridPane.setConstraints(model, 20, 20);
     
     ad = new Button("add");
     ad.getStyleClass().add("ad");
     GridPane.setConstraints(ad, 20, 25);
     ad.setOnAction(new EventHandler(){

         @Override
         public void handle(Event y) {
          
            
          try{
             int chassis = Integer.parseInt(chasi.getText());
           
             String mk = make.getText();
           
             String mode = model.getText();
           
             Stock stk = new Stock(chassis,mk,mode);
           
             
             
             db.addCar(stk);
           
             chasi.setText("");
             make.setText("");
             model.setText("");
            
 
          }catch(NumberFormatException e){
              
              System.out.println(e + "what");
          }
           
             
             
             
         }
         
         
         
         
         
     });
     
     
     gp.getChildren().addAll(lb,chasi,make,model,ad);
     
    
     
     
     //request scene
     
         
     gp2 = new GridPane();;//center
     
     gp2.getStyleClass().add("gp");
     
    
     gp2.setPadding(new Insets(50));
     
     gp2.setHgap(15.0);
     gp2.setVgap(5.0);
     
     lb2 = new Label("Request details");
     GridPane.setConstraints(lb2,20,2);
     lb2.getStyleClass().add("tx");
     
     
     make2 = new TextField();
     make2.setPromptText("enter car make");
     make2.setFocusTraversable(false);
     GridPane.setConstraints(make2, 20, 5);
     
     model2 = new TextField();
     model2.setPromptText("enter car model");
     model2.setFocusTraversable(false);
      GridPane.setConstraints(model2, 20, 10);
      
     tg = new ToggleGroup();
     
     
      
     rb = new RadioButton();
     rb.setText("5 in qty");
     GridPane.setConstraints(rb,20 ,15);
     rb.setOnAction(new EventHandler(){
         
      public void handle(Event t){
          
          
          
      }   
         
         
     });
     rb.setToggleGroup(tg);
     
      
     rb2 = new RadioButton();
     rb2.setText("10 in qty");
     GridPane.setConstraints(rb2,20,16);
     rb2.setToggleGroup(tg);
   
     ad = new Button("request");
     ad.getStyleClass().add("ad");
     GridPane.setConstraints(ad, 20, 20);
     
     
     gp2.getChildren().addAll(lb2,model2,make2,rb,rb2,ad);
    
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     Scene sc = new Scene(bp,1000,720);
     
     sc.getStylesheets().add(getClass().getResource("inventory.css").toExternalForm());
     
     
     stg.setScene(sc);
     stg.show();
       
       
       
   }
    
    
}
